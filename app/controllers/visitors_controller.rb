class VisitorsController < ApplicationController
  def index
    @visitors = Visitor.first_page(params[:creator_id])
    render json: @visitors
  end

  def create
    @visitor = Visitor.create(visitor_params)
    if @visitor.valid?
      render plain: "Record created", status: 200
    else
      render plain: "Error creating new record", status: 500
    end
  end

  def destroy
    @visitor = Visitor.find_by(id: params[:id])
    if @visitor.present? && params[:creator_id] == @visitor.creator_id
      @visitor.destroy
      render plain: "Record destroyed", status: 200
    else
      render plain: "404 Not found", status: 404
    end
  end

  def search
    @visitors = Visitor.search(params[:q], params[:creator_id])
    render json: @visitors
  end

  private

  def visitor_params
    params.permit(:name, :message, :creator_id)
  end
end
