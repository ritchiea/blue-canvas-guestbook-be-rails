class Visitor < ApplicationRecord
  validates_presence_of :name, :message

  SEARCH = "lower(name) LIKE ? or lower(message) LIKE ?"
  TIME_FORMAT = "%l:%M%P %B %-d, %Y"

  def self.first_page(session_id=nil)
    Visitor.order(created_at: :desc).limit(10).map do |visitor|
      visitor.as_safe_json(session_id)
    end
  end

  def self.search(query, session_id=nil)
    query = query.downcase
    visitors = Visitor.
      where(SEARCH, "%#{query}%", "%#{query}%").
      order(created_at: :desc)
    visitors.map {|visitor| visitor.as_safe_json(session_id)}
  end

  def as_safe_json(session_id)
    if session_id != creator_id
      as_json.except("creator_id")
    else
      as_json
    end
  end

  def created_at
    super.strftime(TIME_FORMAT)
  end
end
