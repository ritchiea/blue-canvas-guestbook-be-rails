class AddCreatorIdToVisitor < ActiveRecord::Migration[6.0]
  def change
    add_column :visitors, :creator_id, :string, unique: true
  end
end
