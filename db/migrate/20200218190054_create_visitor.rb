class CreateVisitor < ActiveRecord::Migration[6.0]
  def change
    create_table :visitors do |t|
      t.string :name
      t.text :message
      t.timestamps
    end
  end
end
