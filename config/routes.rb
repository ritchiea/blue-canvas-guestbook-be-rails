Rails.application.routes.draw do
  resources :visitors, only: [:new, :index, :create, :destroy]
  get "visitors/search", to: "visitors#search"
end
