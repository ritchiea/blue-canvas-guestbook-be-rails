# README

MacOS setup instructions for the Guestbook backend:

1. Install Ruby 2.6.3 (preferably via RVM)
2. Run `gem install bundler`
3. Clone the repo from Gitlab if you have not already
2. You may wish to create `.ruby-version` and `.ruby-gemset` files in the project folder
4. In the project folder run `bundle`
4. Install sqlite3 via homebrew on MacOS or your favorite Linux package manager
5. Run `rake db:create`
6. Run `rake db:migrate`
7. Run `rails s -p 3001`
